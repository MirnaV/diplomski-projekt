import math
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2
import numpy as np
from sensor_msgs.msg import PointCloud2
import ros_numpy
import os
import tf2_ros
import numpy as np
from sensor_msgs.msg import Image
from utils.point_utils import apply_transformations
from utils.blob_detector_params import set_detector_params
import matplotlib.pyplot as plt

class PointCloudListener:
    def __init__(self, topic, image_center):
        self.image_center = image_center
        self.topic = topic
        self.center = None

    def get_point_cloud_center(self):
        message = rospy.wait_for_message(self.topic, PointCloud2, timeout=None)
        pc = ros_numpy.point_cloud2.pointcloud2_to_array(message)
        y = round(self.image_center[0].pt[0])
        x = round(self.image_center[0].pt[1])
        self.center = pc[x][y]


class ImageListener:
    def __init__(self, topic):
        self.bridge = CvBridge()
        self.topic = topic

        self.center = None

    def detectBlobs(self, mask):
        detector = set_detector_params()
        self.center = detector.detect(mask)
        keypoints_radius = []
        for keypoint in self.center:
            keypoints_radius.append(keypoint.size)

        im_with_keypoints = cv2.drawKeypoints(mask, self.center, np.array([]), (0, 0, 255),
                                              cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        
        return im_with_keypoints

    def get_center(self):
        message = rospy.wait_for_message(self.topic, Image, timeout=None)
        try:
            cv_image = self.bridge.imgmsg_to_cv2(message, message.encoding)
            cv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
            cv_image = np.asanyarray(cv_image)
            #cv2.imshow("Image", cv_image)
            truncated_depth = self.detectBlobs(cv_image)
            #cv2.imshow('Truncated Depth', truncated_depth)
            cv2.imwrite("./Mirna_blob/realsense_blob_detected.jpg", truncated_depth)
            k = cv2.waitKey(1) & 0xFF
            if k == 27:
                exit(0)

        except CvBridgeError as e:
            print(e)
            return


class TF2Listener:
    def __init__(self, camera_frame):
        self.camera_frame = camera_frame
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
    
    def get_transformations(self):
        trans = []
        ms_frame = "ms_red_band"
        for i in range(1, 6):
            name = ms_frame + str(i)
            try:
                t = self.tf_buffer.lookup_transform(
                    name, self.camera_frame, rospy.Time(), rospy.Duration(4.0))
                trans.append(t)
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                print("Did not get transform")

        ms_frame = "ms_blue_band"
        for i in range(1, 6):
            name = ms_frame + str(i)
            try:
                t = self.tf_buffer.lookup_transform(
                    name, self.camera_frame, rospy.Time(), rospy.Duration(4.0))
                trans.append(t)
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                print("Did not get transform")
        
        return trans



def main():
    rospy.init_node("depth_image_processor")
    point_topic = '/camera/depth_registered/points'
    image_topic = "/camera/color/image_raw"
    camera_frame = 'panda_camera'

    image_listener = ImageListener(image_topic)
    image_listener.get_center()

    point_cloud_listener = PointCloudListener(point_topic, image_listener.center)
    point_cloud_listener.get_point_cloud_center()
    print("Point cloud: ", point_cloud_listener.center)

    tf2_listener = TF2Listener(camera_frame)
    trans = tf2_listener.get_transformations()
    transformed_center = apply_transformations(point_cloud_listener.center, trans)

    errors = []
    files = os.listdir("./Mirna_blob/")
    for file in files:
        if "2022" in file or "detected" in file or "calculated" in file:
            continue

        center = transformed_center[file]
        file = "./Mirna_blob/" + file
        im = cv2.imread(file, cv2.IMREAD_COLOR)
        detector = set_detector_params(minArea=120, maxArea=10000)
        keypoints = detector.detect(im)
        keypoint = []
        keypoint.append(cv2.KeyPoint(x=center[0], y=center[1], _size=100.0))
        im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                                cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
                                                
        im_with_keypoints2 = cv2.drawKeypoints(im, keypoint, np.array([]), (0, 0, 255),
                                                cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        
        cv2.imwrite(file[:-4]+"_detected.jpg", im_with_keypoints)
        cv2.imwrite(file[:-4]+"_calculated.jpg", im_with_keypoints2)

        error = math.sqrt((center[0] - keypoints[0].pt[0])**2 + (center[1] - keypoints[0].pt[1])**2)
        errors.append(error)
        print("Detected center: ", keypoints[0].pt)
        print("Calculated center: ", center)
        print("Error: ", error)
        print()
        #cv2.imshow("Detected Blobs", im_with_keypoints)
        #cv2.imshow("Calculated Blobs", im_with_keypoints2)
        #cv2.waitKey(27)

    x_axis = []
    for i in range(len(errors)):
        x_axis.append(i + 1)
    plt.xlim(0, 12)
    plt.stem(x_axis, errors, use_line_collection=True)
    plt.show()
            

if __name__ == "__main__":
    main()